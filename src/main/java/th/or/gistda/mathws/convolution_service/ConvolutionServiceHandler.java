package th.or.gistda.mathws.convolution_service;

import java.util.ArrayList;
import java.util.List;

import th.or.gistda.mathws.base.HttpConst;
import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;
import th.or.gistda.mathws.service_selector.IServiceHandler;

public class ConvolutionServiceHandler implements IServiceHandler {

	@Override
	public void handle(IMathWsRequest req, IMathWsResponse res) {
		
		if (!req.getOperation().equals("normal")) {
			res.setService(req.getName(), req.getOperation());
			res.setHttp(HttpConst.STATUS_BAD_REQUEST, "Invalid convolution service operation");
			return;
		}
		
		List<Double> inputA = req.getInputA();
		List<Double> inputB = req.getInputB();
		List<Double> outputC = new ArrayList<Double>();
		
		convolve_normal(inputA, inputB, outputC);
		
		res.setService(req.getName(), req.getOperation());
		res.setHttp(HttpConst.STATUS_OK, "Operation completed successfully");
		res.setInput(inputA, inputB);
		res.setOutput(outputC);
	}
	
	protected void convolve_normal(List<Double> inputA, List<Double> inputB, List<Double> outputC) {
		
		int timeLength = inputA.size();
		for (int t=0;t<timeLength;t++) {
			
			Double acc = 0.0;
			for (int idx=0;idx<=t;idx++) {
				acc += inputA.get(idx) * inputB.get((t) - idx);
			}
			
			outputC.add(acc);
		}
	}
	
}
