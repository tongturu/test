package th.or.gistda.mathws.service_selector;

import th.or.gistda.mathws.base.HttpConst;
import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;
import th.or.gistda.mathws.convolution_service.ConvolutionServiceHandler;
import th.or.gistda.mathws.correlation_service.CorrelationServiceHandler;
import th.or.gistda.mathws.simple_service.SimpleServiceHandler;

public class ServiceSelector implements IServiceSelector {

	
	@Override
	public void process(IMathWsRequest req, IMathWsResponse res) {
		
		EService servName = EService.parse(req.getName());
		
		if (servName == null) {
			res.setService(req.getName(), req.getOperation());
			res.setHttp(HttpConst.STATUS_BAD_REQUEST, "Invalid service name");
			return;
		}
		
		IServiceHandler servHdl = null;
		
		switch (servName) {
		case simple:
			servHdl = createSimpleServiceHandler();
			break;
		case correlation:
			servHdl = createCorrelationServiceHandler();
			break;
		case convolution:
			servHdl = createConvolutionServiceHandler();
			break;
		}
		
		servHdl.handle(req, res);
		
	}
	
	protected IServiceHandler createSimpleServiceHandler() {
		return new SimpleServiceHandler();
	}
	
	protected IServiceHandler createCorrelationServiceHandler() {
		return new CorrelationServiceHandler();
	}
	
	protected IServiceHandler createConvolutionServiceHandler() {
		return new ConvolutionServiceHandler();
	}
}
