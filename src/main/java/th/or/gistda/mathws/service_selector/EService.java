package th.or.gistda.mathws.service_selector;

public enum EService {
	simple("simple"),
	correlation("correlation"),
	convolution("convolution");
	
	private String name;
	
	EService(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public static EService parse(String str) {
		if (str.equals(simple.getName()))
			return simple;
		if (str.equals(correlation.getName()))
			return correlation;
		if (str.equals(convolution.getName()))
			return convolution;
		return null;
	}

}
