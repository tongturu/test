package th.or.gistda.mathws.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import th.or.gistda.mathws.service_selector.IServiceSelector;
import th.or.gistda.mathws.service_selector.ServiceSelector;

public class BaseMain {

	    public static void main(String[] args) throws Exception {
            System.out.println("GISTDA MathWs starting  .  .  .");
	        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
	        server.createContext("/mathapi", new EntryHandler());
	        server.setExecutor(null);
	        server.start();
	    }

	    static class EntryHandler implements HttpHandler {
	    	
	    	private IServiceSelector serveSel = new ServiceSelector();
	    	
	        @Override
	        public void handle(HttpExchange ex) throws IOException {
	        	
	        	IMathWsRequest request = null;
	        	IMathWsResponse response = null;
	        	String httpMethod = getRequestMethod(ex);
	        	int rStatus = HttpConst.STATUS_OK;
	        	String rResponse = "This text is not valid";
	        	
	        	switch (httpMethod) {
	        	case HttpConst.METHOD_POST:
	        		try {
	        			request = preprocessRequest(ex);
	        		}
	        		catch (Exception e) {
	        			response = new MathWsResponse();
	        			response.setHttp(HttpConst.STATUS_BAD_REQUEST, e.getMessage());
	        			break;
	        		}
	        		
	        		response = new MathWsResponse();
	        		this.serveSel.process(request, response);
	        		break;
	        	default:
	        		response = new MathWsResponse();
	        		response.setHttp(HttpConst.STATUS_METHOD_NOT_ALLOWED, "This HTTP method is not allowed");
	        		break;
	        	}
	        	
	        	rStatus = response.getHttpStatus();
	        	rResponse = response.toHttpBody();
	        	
	            ex.sendResponseHeaders(rStatus, rResponse.length());
	            OutputStream os = ex.getResponseBody();
	            os.write(rResponse.getBytes());
	            os.close();
	        }
	        
	        private String getRequestMethod(HttpExchange ex) {
	        	return ex.getRequestMethod().toUpperCase();
	        }
	        
	        private IMathWsRequest preprocessRequest(HttpExchange ex) throws Exception {
	        	
	        	String hReq = getRequestBody(ex);
	        	IMathWsRequest mReq = new MathWsRequest();
        		RequestValidator.parseRawRequest(hReq, mReq);
        		
        		return mReq;
	        }
	        
	        private String getRequestBody(HttpExchange ex) {
	        	InputStream is = ex.getRequestBody();
	        	
	        	StringBuilder requestBuilder = new StringBuilder();
	        	String line = null;
	        	
	        	try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is))) {	
	        		while ((line = bufferedReader.readLine()) != null) {
	        			requestBuilder.append(line);
	        		}
	        	}
	        	catch (Exception e) {
	        		
	        	}
	        	return requestBuilder.toString();
	        }
	    }

}
