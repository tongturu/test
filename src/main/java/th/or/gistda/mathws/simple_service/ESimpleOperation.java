package th.or.gistda.mathws.simple_service;

import th.or.gistda.mathws.base.IMathWsOperation;

public enum ESimpleOperation implements IMathWsOperation {
	add("add"), sub("sub"), mul("mul"), div("div");
	
	private String name;
	
	ESimpleOperation(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public static IMathWsOperation parse(String str) {
		if (str.equals(add.getName()))
			return add;
		if (str.equals(sub.getName()))
			return sub;
		if (str.equals(mul.getName()))
			return mul;
		if (str.equals(div.getName()))
			return div;
		
		return null;
	}
}
